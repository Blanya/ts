var fstColor = document.getElementById("fst_color");
var scdColor = document.getElementById("scd_color");
var thdColor = document.getElementById("thd_color");
var btnSend = document.getElementById("btn_send");
var info = document.getElementById("info_ohms");
//Tableau contenant les valeurs des couleurs dans l'enum colors
var values;
//Nom des couleurs, value des input
var result;
//Flag si une valeur est fausse
var reelValue;
var colors;
(function (colors) {
    colors[colors["noir"] = 0] = "noir";
    colors[colors["marron"] = 1] = "marron";
    colors[colors["rouge"] = 2] = "rouge";
    colors[colors["orange"] = 3] = "orange";
    colors[colors["jaune"] = 4] = "jaune";
    colors[colors["vert"] = 5] = "vert";
    colors[colors["bleu"] = 6] = "bleu";
    colors[colors["violet"] = 7] = "violet";
    colors[colors["gris"] = 8] = "gris";
    colors[colors["blanc"] = 9] = "blanc";
})(colors || (colors = {}));
function getColors(col, name) {
    var value = "";
    switch (name) {
        case col[0]:
            value = col.noir;
            break;
        case col[1]:
            value = col.marron;
            break;
        case col[2]:
            value = col.rouge;
            break;
        case col[3]:
            value = col.orange;
            break;
        case col[4]:
            value = col.jaune;
            break;
        case col[5]:
            value = col.vert;
            break;
        case col[6]:
            value = col.bleu;
            break;
        case col[7]:
            value = col.violet;
            break;
        case col[8]:
            value = col.gris;
            break;
        case col[9]:
            value = col.blanc;
            break;
        default:
            reelValue = false;
            console.log("Cette couleur de résistance n'existe pas");
            break;
    }
    values.push(value);
}
function ask() {
    //initialisation
    values = [];
    result = [];
    //flag manque de résistance 
    reelValue = true;
    while (info.firstChild) {
        info.removeChild(info.firstChild);
    }
    result.push(fstColor.value.toLowerCase(), scdColor.value.toLowerCase(), thdColor.value.toLowerCase());
    //lowerCase comme ds l'enum
    result.forEach(function (color) {
        return getColors(colors, color.toLowerCase());
    });
    giveOhms();
}
function giveOhms() {
    var units = "ohms";
    if (reelValue) {
        var resultat = void 0;
        var numberToString = values[0].toString().concat(values[1].toString());
        var lastValue = values[2];
        if (typeof lastValue == "number" && lastValue >= 1) {
            var exp = (Math.pow(10, lastValue));
            resultat = Number(numberToString) * exp;
        }
        else {
            resultat = Number(numberToString);
        }
        var p = document.createElement("p");
        if (resultat > 1000) {
            units = "kiloohms";
            resultat = resultat / 1000;
        }
        p.textContent = "Le résultat des résistances " + result + " est de " + resultat + " " + units;
        info.appendChild(p);
    }
    else {
        var p = document.createElement("p");
        p.textContent = "Une ou plusieurs couleurs sont erronée(s)";
        info.appendChild(p);
    }
}
btnSend.addEventListener("click", ask);
