let fstColor : HTMLInputElement = document.getElementById("fst_color") as HTMLInputElement;
let scdColor : HTMLInputElement = document.getElementById("scd_color") as HTMLInputElement;
let thdColor : HTMLInputElement = document.getElementById("thd_color") as HTMLInputElement;
let btnSend : HTMLButtonElement = document.getElementById("btn_send") as HTMLButtonElement;
let info : HTMLDivElement = document.getElementById("info_ohms") as HTMLDivElement;

type StNum = string | number;
type Units = "ohms" | "kiloohms";

//Tableau contenant les valeurs des couleurs dans l'enum colors
let values : StNum[];

//Nom des couleurs, value des input
let result : string[];

//Flag si une valeur est fausse
let reelValue: boolean;

enum colors 
{
    noir,
    marron,
    rouge,
    orange,
    jaune, 
    vert,
    bleu, 
    violet,
    gris,
    blanc,
}

function getColors(col, name: string)
{
    let value : StNum = "";

    switch (name) {
        case col[0]:
            value = col.noir;
            break;
        case col[1]:
            value = col.marron;
            break;
        case col[2]:
            value = col.rouge;
            break;
        case col[3]:
            value = col.orange;
            break;
        case col[4]:
            value = col.jaune;
            break;
        case col[5]:
            value = col.vert;
            break;
        case col[6]:
            value = col.bleu;
            break;
        case col[7]:
            value = col.violet;
            break;
        case col[8]:
            value = col.gris;
            break;
        case col[9]:
            value = col.blanc;
            break;    
        default:
            reelValue = false;
            console.log("Cette couleur de résistance n'existe pas");
            break;
    }

    values.push(value);    
}

function ask()
{
    //initialisation
    values = [];
    result = [];
    
    //flag manque de résistance 
    reelValue = true;

    while(info.firstChild)
    {
        info.removeChild(info.firstChild);    
    }

    result.push(fstColor.value.toLowerCase(), scdColor.value.toLowerCase(), thdColor.value.toLowerCase());    

    //lowerCase comme ds l'enum
    result.forEach(color =>
        getColors(colors, color.toLowerCase()));
    
    giveOhms();
}


function giveOhms()
{   
    let units : Units = "ohms";

    if(reelValue)
    {
        let resultat : number;
        let numberToString : StNum = values[0].toString().concat(values[1].toString());
        
        let lastValue : StNum = values[2];

        if(typeof lastValue == "number" && lastValue>=1)
        {
            let exp: number = (Math.pow(10, lastValue));
            resultat = Number(numberToString) * exp;
        }
        else
        {
            resultat = Number(numberToString);
        }

        let p = document.createElement("p");
        if(resultat>1000)
        {
            units = "kiloohms";
            resultat = resultat / 1000;
        }
        p.textContent = "Le résultat des résistances " + result + " est de " + resultat + " " + units;
        info.appendChild(p);    
    }
    else 
    {
        let p = document.createElement("p");
        p.textContent = "Une ou plusieurs couleurs sont erronée(s)";
        info.appendChild(p);
    }
}

btnSend.addEventListener("click", ask);