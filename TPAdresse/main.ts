let fstAdress = document.getElementById("fst_adress") as HTMLInputElement;
let scdAdress = document.getElementById("scd_adress") as HTMLInputElement;
let city = document.getElementById("city") as HTMLInputElement;
let adress = document.getElementById("adress");
let btn = document.getElementById("save_btn")


//OPTIONNAL ADRESS 2
function writeAdress (arg : string, arg3 : string, arg2? : string)
{
    while(adress?.firstChild)
    {
        adress?.removeChild(adress?.firstChild)
    }
    let p = document.createElement("p");
    p.textContent = arg + "," + arg2 + ", " + arg3;
    adress?.appendChild(p);
}

function groupAdress()
{
    writeAdress(fstAdress?.value, city?.value, scdAdress?.value);  
}

btn?.addEventListener("click", groupAdress);


//Paramètres par défaut 
function writeAdress2 (arg : string, arg3 : string, arg2 = "N/A")
{
    while(adress?.firstChild)
    {
        adress?.removeChild(adress?.firstChild)
    }
    let p = document.createElement("p");
    p.textContent = "Adress: " + arg + " Adress2: " + arg2 + " City: " + arg3;
    console.log(arg2)
    return p;
}

function groupAdress2()
{
    if(scdAdress?.value == "")
    {
        let fullAdress = writeAdress2(fstAdress?.value, city?.value);   
        adress?.appendChild(fullAdress)   
    }
    else
    {
        let fullAdress = writeAdress2(fstAdress?.value, city?.value, scdAdress?.value);  
        adress?.appendChild(fullAdress)    
    }
}

//btn?.addEventListener("click", groupAdress2);



// REST
function writeAdress3(...restAdress : string[])
{
    while(adress?.firstChild)
    {
        adress?.removeChild(adress?.firstChild)
    }

    let p = document.createElement("p");

    for(let i in restAdress)
    {
        if(i=='2')
        {
            p.textContent += " " + restAdress[i]; 
        }
        else 
        {
            p.textContent += " " + restAdress[i] + ",";     
        }   
    }
    adress?.appendChild(p);
}

function groupAdress3()
{
    writeAdress3(fstAdress?.value, scdAdress?.value, city?.value);
}


//btn?.addEventListener("click", groupAdress3);