var fstAdress = document.getElementById("fst_adress");
var scdAdress = document.getElementById("scd_adress");
var city = document.getElementById("city");
var adress = document.getElementById("adress");
var btn = document.getElementById("save_btn");
//OPTIONNAL ADRESS 2
function writeAdress(arg, arg3, arg2) {
    while (adress === null || adress === void 0 ? void 0 : adress.firstChild) {
        adress === null || adress === void 0 ? void 0 : adress.removeChild(adress === null || adress === void 0 ? void 0 : adress.firstChild);
    }
    var p = document.createElement("p");
    p.textContent = arg + "," + arg2 + ", " + arg3;
    adress === null || adress === void 0 ? void 0 : adress.appendChild(p);
}
function groupAdress() {
    writeAdress(fstAdress === null || fstAdress === void 0 ? void 0 : fstAdress.value, city === null || city === void 0 ? void 0 : city.value, scdAdress === null || scdAdress === void 0 ? void 0 : scdAdress.value);
}
btn === null || btn === void 0 ? void 0 : btn.addEventListener("click", groupAdress);
//Paramètres par défaut 
function writeAdress2(arg, arg3, arg2) {
    if (arg2 === void 0) { arg2 = "N/A"; }
    while (adress === null || adress === void 0 ? void 0 : adress.firstChild) {
        adress === null || adress === void 0 ? void 0 : adress.removeChild(adress === null || adress === void 0 ? void 0 : adress.firstChild);
    }
    var p = document.createElement("p");
    p.textContent = "Adress: " + arg + " Adress2: " + arg2 + " City: " + arg3;
    console.log(arg2);
    return p;
}
function groupAdress2() {
    if ((scdAdress === null || scdAdress === void 0 ? void 0 : scdAdress.value) == "") {
        var fullAdress = writeAdress2(fstAdress === null || fstAdress === void 0 ? void 0 : fstAdress.value, city === null || city === void 0 ? void 0 : city.value);
        adress === null || adress === void 0 ? void 0 : adress.appendChild(fullAdress);
    }
    else {
        var fullAdress = writeAdress2(fstAdress === null || fstAdress === void 0 ? void 0 : fstAdress.value, city === null || city === void 0 ? void 0 : city.value, scdAdress === null || scdAdress === void 0 ? void 0 : scdAdress.value);
        adress === null || adress === void 0 ? void 0 : adress.appendChild(fullAdress);
    }
}
//btn?.addEventListener("click", groupAdress2);
// REST
function writeAdress3() {
    var restAdress = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        restAdress[_i] = arguments[_i];
    }
    while (adress === null || adress === void 0 ? void 0 : adress.firstChild) {
        adress === null || adress === void 0 ? void 0 : adress.removeChild(adress === null || adress === void 0 ? void 0 : adress.firstChild);
    }
    var p = document.createElement("p");
    for (var i in restAdress) {
        if (i == '2') {
            p.textContent += " " + restAdress[i];
        }
        else {
            p.textContent += " " + restAdress[i] + ",";
        }
    }
    adress === null || adress === void 0 ? void 0 : adress.appendChild(p);
}
function groupAdress3() {
    writeAdress3(fstAdress === null || fstAdress === void 0 ? void 0 : fstAdress.value, scdAdress === null || scdAdress === void 0 ? void 0 : scdAdress.value, city === null || city === void 0 ? void 0 : city.value);
}
//btn?.addEventListener("click", groupAdress3);
