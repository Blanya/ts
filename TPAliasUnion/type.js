//Exercice1
function sum(arg) {
    var res = 0;
    if (typeof arg == "number") {
        res = arg;
    }
    else {
        for (var i = 0; i < arg.length; i++) {
            res += arg[i];
        }
    }
    console.log(res);
}
sum(3);
sum([3, 15, 2]);
var intVariable = 25;
var floatVariable = 10.25;
var stringVariable = ["Bonjour", "Paul", "Bye"];
var numberVariable = [14, 85, 20, 92];
console.log(intVariable);
console.log(floatVariable);
console.log(stringVariable);
console.log(numberVariable);
