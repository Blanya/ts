//Exercice1

function sum(arg : (number | number[]))
{
    let res : number = 0;

    if(typeof arg == "number")
    {
        res = arg;
    }
    else
    {
        for (let i = 0; i < arg.length; i++) 
        {
            res += arg[i];
        }
    }
    console.log(res);
}

sum(3);
sum([3,15,2]);


//Exercice 2

type int = number;
type float = number;

type StringArray = string[];
type NumberArray = number[];

let intVariable : int = 25;
let floatVariable : float = 10.25;

let stringVariable : StringArray = ["Bonjour", "Paul", "Bye"];
let numberVariable : NumberArray = [14, 85, 20, 92];

console.log(intVariable);
console.log(floatVariable);
console.log(stringVariable);
console.log(numberVariable);