//Ex 1.4

const integer : number = 6;
const float : number = 6.66;
const hex : number= 0xf00d;
const binary : number = 0b1010011010;
const octal : number = 0o744;
const negZero : number = -0;
const actualNumber : number = NaN;
const largestNumber : number= Number.MAX_VALUE;
const mostBiglyNumber : number = Infinity; 

const members: number [] =[
    integer,
    float,
    hex,
    binary,
    octal,
    negZero,
    actualNumber,
    largestNumber,
    mostBiglyNumber
]

members[0] = 12345;

//Ex 1.5

const sequence : number[] = Array.from(Array(10).keys());
const animals : string[] = ["Pangolin", "Aardvark", "Echidna", "Binturong"];
const stringAndNumbers : (string | number)[]= [1, "one", 2, "two", 3, "three"];
const allMyArrays : (string | number)[][]= [sequence, animals, stringAndNumbers];

//Ex 1.6

const inventoryItem : [string, number]= ['fidget wibbit', 11];

const [nom, qty] = inventoryItem; 

const msg = addInventory(nom, qty);

console.log("Exercice 1.6", msg);

function addInventory(name: string, quantity: number) : string
{
    return `Added ${quantity} ${name}(s) to inventory`;
}