var inputNb = document.getElementById("input_nb");
var info = document.getElementById("info");
var btnSend = document.getElementById("btn_send");
var nb;
var exp;
var values;
var resultat;
var msg;
function nbAmstrong() {
    //initialisation
    exp = 0;
    values = [];
    resultat = 0;
    msg = "";
    nb = inputNb.value;
    while (info.firstChild) {
        info.removeChild(info.firstChild);
    }
    //division nb
    if (typeof nb == "string") {
        exp = nb.length;
        values = nb.split("");
    }
    //expo nb
    for (var i = 0; i < values.length; i++) {
        if (i == values.length - 1) {
            msg += "".concat(values[i], " ^ ").concat(exp);
            console.log(msg);
        }
        else {
            msg += " ".concat(values[i], " ^ ").concat(exp, " +");
            console.log(msg);
        }
        resultat += Math.pow(Number(values[i]), exp);
    }
    //afficher
    var p = document.createElement("p");
    //comparer
    if (Number(nb) == resultat) {
        p.textContent = nb + " est un nombre Amstrong, car " + nb + " = " + msg + " = " + resultat;
        info.appendChild(p);
    }
    else {
        p.textContent = nb + " n'est pas un nombre Amstrong, car " + nb + " != " + msg + " = " + resultat;
        info.appendChild(p);
    }
}
btnSend.addEventListener("click", nbAmstrong);
