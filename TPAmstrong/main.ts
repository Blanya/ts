let inputNb : HTMLInputElement = document.getElementById("input_nb") as HTMLInputElement;
let info : HTMLDivElement = document.getElementById("info") as HTMLDivElement;
let btnSend : HTMLButtonElement = document.getElementById("btn_send") as HTMLButtonElement;

// alias nb ou str
type NbStr = number | string;

let nb : NbStr;
let exp : number;
let values : NbStr[];
let resultat : number;
let msg : string;


function nbAmstrong()
{
    //initialisation
    exp = 0;
    values = [];
    resultat = 0;
    msg = "";
    nb = inputNb.value;
    while(info.firstChild)
    {
        info.removeChild(info.firstChild);
    }

    //division nb
    if(typeof nb == "string")
    {
        exp = nb.length; 
        values = nb.split("");    
    }
    
    //expo nb
    for (let i = 0; i < values.length; i++) 
    {
        if(i == values.length-1)
        {
            msg += `${values[i]} ^ ${exp}`;
            console.log(msg)
        }
        else
        {
            msg += ` ${values[i]} ^ ${exp} +`; 
            console.log(msg)   
        }

        resultat += Math.pow(Number(values[i]), exp);
    }

    //afficher
    let p = document.createElement("p");

    //comparer
    if(Number(nb) == resultat)
    {
        p.textContent = nb + " est un nombre Amstrong, car " + nb + " = " + msg + " = " + resultat;
        info.appendChild(p)
    } 
    else
    {
        p.textContent = nb + " n'est pas un nombre Amstrong, car " + nb + " != " + msg + " = " + resultat;
        info.appendChild(p);
    }
}

btnSend.addEventListener("click", nbAmstrong);